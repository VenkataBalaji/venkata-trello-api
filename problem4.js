
let https=require("https")
let { apiToken, apiKey } = require("./utils");

function getCards(listId){
    urlPath=`https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`
    return new Promise((resolve,reject)=>{
        https.get(urlPath,(response)=>{
            let data=""
            response.on("data",(chunk)=>{
                 data=data+chunk
            })
            response.on("end",()=>{
                try {
                    resolve(JSON.parse(data))
                } catch (error) {
                    reject(new Error("Invalid Id"))
                }
                
            })
             response.on("error",(error)=>{
                reject(error)
             })

        })
    })
}

module.exports=getCards