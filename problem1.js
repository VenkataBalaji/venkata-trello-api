
let https = require("https");

let {apiToken,apiKey}=require("./utils")

function getBoardId(boardId) {

  let urlPath =  `https://api.trello.com/1/boards/${boardId}/memberships?key=${apiKey}&token=${apiToken}`;
  return new Promise((resolve, reject) => {
    https.get(urlPath, (response) => {
      let data = "";
      response.on("data", (chunk) => {
        data = data + chunk;
      });
      response.on("end", () => {
        try{
        let res = JSON.parse(data);
        resolve(res)
        }catch(error){
          reject(new Error ("Invalid Board Id"))
        }
        });
        response.on("error", (error) => {
          reject(error);
        });

      });

     
    });
  }
  

module.exports=getBoardId