
let https=require("https")

let {apiToken,apiKey}=require("./utils")
let post={
  method:"POST",
  headers:{
    'Content-Type':'/json'
  }
}

function createBoard(boardName){
let urlPath =`https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`
return new Promise((resolve,reject)=>{
 const request=  https.request(urlPath,post,(res)=>{
  let data=""
    res.on('data',(chunck)=>{
       data=data+chunck
    })
    res.on("end",()=>{
      resolve(JSON.parse(data))
    })
    
 })
 request.on("error",(error)=>{
  reject(error)
})
request.end()

 return request
})
}

module.exports=createBoard