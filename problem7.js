let https = require("https");

let { apiToken, apiKey } = require("./utils");

const options = {
  method: "PUT",
};
function deleteLists(id) {

let urlPath=
    `https://api.trello.com/1/lists/${id}/closed?key=${apiKey}&token=${apiToken}&value=true`

 return new Promise((resolve,reject)=>{
    let request=https.request(urlPath,options,(res)=>{
        let data=""
        res.on('data',(chunck)=>{
           data=data+chunck
        })
        res.on("end",()=>{
          resolve(JSON.parse(data))
        })
        res.on("error",(error)=>{
         reject(error)
       })
     })
     request.end()
    })
  
}
module.exports=deleteLists