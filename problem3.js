let https = require("https");
let { apiToken, apiKey} = require("./utils");
function getList(boardId) {
  let urlPath = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;

  return new Promise((resolve, reject) => {
    https.get(urlPath, (response) => {
      let data = "";
      response.on("data", (chunck) => {
        data = data + chunck;
      });

      response.on("end", () => {
        try {
          let res = JSON.parse(data);
          resolve(res);
        } catch (error) {
          reject(new Error("Invalid Board Id"));
        }
      });

      response.on("error", () => {
        reject(error);
      });
    });
  });
}
module.exports = getList;
