
const{apiKey,apiToken}=require("./utils")



function getCheckItems(checkListID){
    let urlPath=`https://api.trello.com/1/checklists/${checkListID}/checkItems?key=${apiKey}&token=${apiToken}`
  return  fetch(urlPath, {
  method: 'GET'
})
  .then(response => {
    console.log(`Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
 
}

function setStateOfCheckItem(cardId,checkItemId){
    let urlPath=`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}&state=complete`
    return fetch(urlPath, {
  method: 'PUT'
})
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })

}

module.exports={setStateOfCheckItem,getCheckItems}