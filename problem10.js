const { apiKey, apiToken } = require("./utils");

function setStateOfCheckItem(cardId, checkItemId) {
  let urlPath = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}&state=incomplete`;
  setTimeout(() => {
    return fetch(urlPath, {
      method: "PUT",
    }).then((response) => {
      return response.json();
    });
  }, 1000);
}

module.exports = setStateOfCheckItem;
