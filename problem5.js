
let getCards=require("./problem4")
let getList=require("./problem3")
function getAllCards(boardId){
    return new Promise((resolve,reject)=>{
        getList(boardId)
      .then((res)=>{
        let promisearray = res.map((ele)=>getCards(ele.id))
      return  Promise.all(promisearray)
      })
      .then((res)=>{
        resolve(res)
      })
      .catch((error)=>{
        reject(error)
      })
    })
     
}
module.exports=getAllCards