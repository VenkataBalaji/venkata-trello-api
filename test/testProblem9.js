const getAllCards = require("../problem5.js");
const { getCheckItems, setStateOfCheckItem } = require("../problem9.js");

let object = {};
getAllCards("ejo8PdNn")
  .then((cardsData) => {
    cardsData = cardsData.flat();
    let arrayOfPromiseOfGetCheckItems = [];
    cardsData.forEach((card) => {
      object[card.id] = card.idChecklists;
      card.idChecklists.forEach((listsId) => {
        arrayOfPromiseOfGetCheckItems.push(getCheckItems(listsId));
      });
    });

    return Promise.all(arrayOfPromiseOfGetCheckItems);
  })
  .then((getCheckItemData) => {
    getCheckItemData = getCheckItemData.flat();
    console.log(getCheckItemData);
    let promiseArray = getCheckItemData.map((checkItem) => {
      let cardId = Object.keys(object).find((id) => {
        return object[id].includes(checkItem.idChecklist);
      });

      return setStateOfCheckItem(cardId, checkItem.id);
    });
    return Promise.all(promiseArray);
  })
  .then((res) => {
    console.log(res);
  })
  .catch((error) => {
    console.log(error);
  });
